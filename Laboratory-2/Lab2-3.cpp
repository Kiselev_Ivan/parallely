// copyright by Ivan Kiselev
// �3 - ��������� ��������

#include <mpi.h> 
#include <iostream>
#include <windows.h>

const int HMEat = 3;	// ������� ��� ������ ������ ��������

void Eat(int number) {
	Sleep(3000);
	std::cout << "philosopher number " << number << " end eat" << std::endl;
	Sleep(500);
}

void Think(int number) {
	Sleep(4000);
	std::cout << "philosopher number " << number << " end think" << std::endl;
	Sleep(500);
}

int main(int argc, char* argv[]) {
	int procs_count;		// 0 ������� ���������� ��������������
	int procs_rank;
	bool buf = false;
	int numb_buf = -1;

	int times = HMEat;	// ��������� ����������

	MPI_Request Rst;
	MPI_Status st;	
	MPI_Init(&argc,&argv);
	
	MPI_Comm_size(MPI_COMM_WORLD, &procs_count);
	// ���-�� ���������
	MPI_Comm_rank(MPI_COMM_WORLD, &procs_rank);
	// ���� ��������
	std::cout << "Proc : " << procs_rank << " Started" << std::endl;
	Sleep(500);
	if (procs_count <= 2) {
		std::cout << "We haven't 2 spoons";
	}
	else {
		if (procs_rank == 0) {
			Sleep(500);
			bool* spoon_busy = new bool(procs_count - 1);		// ������ ��������� �������� ��������
			double* priority = new double(procs_count - 1);		// ������ ����������� ���������			
			for (int i = 0; i < procs_count - 1; i++) {
				spoon_busy[i] = false;							// � ����� ������ ����� �� ������
				priority[i] = 0.99 - i*0.01;					// ��������� ������� �� ������ ��������, �������� ��� ���-�� ��������� > 99
			}			


			int k = 0;
			
			int* food;
			food = new int(procs_count + 1);
			for (int i = 0; i < procs_count + 1; i++) {
				food[i] = HMEat;
			}

			while (k < HMEat * (procs_count - 1) ) {		// ���� ���� ��� �� �������� ����������� ����� ��� ( 0 ������� �� ������)
				for (int i = 1; i < procs_count; i++) {
					times = food[i];
					if ((food[i] > 0) && (k < HMEat * (procs_count - 1)))
						MPI_Recv(&numb_buf, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &st);		// ���� ��� ����� �������� �������� ���
					if (numb_buf != -1) {
						if (numb_buf > -1) {											// �� ������ �� ��������� �����
							if (!spoon_busy[numb_buf]) {								// ���� ����� �� ������
								if (numb_buf != 0) {									// ���� ��� �� ������� �����(��������� ����, ��� �������� ����� � �����)
									if (priority[i - 1] + 1 > 2) 					 	//�� ���� ����� ������ ������� ������� �����, �� �� ������ ����,
									{
										priority[i - 1] ++;
										spoon_busy[numb_buf] = true;
										buf = true;
										MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
									}
									else {
										if (i != numb_buf) {		// ���� ��� ����� �����
											if (priority[i - 2] + 1 < 2) 					// ��� ���� ��� ����� ���� ����� ���� �� ������
											{
												priority[i - 1] ++;
												spoon_busy[numb_buf] = true;
												buf = true;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}
											else {
												buf = false;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}
										}
										else {					// ������ �����
											if (priority[i] + 1 < 2) 					// ��� ���� ��� ����� ���� ����� ���� �� ������
											{
												priority[i - 1] ++;
												spoon_busy[numb_buf] = true;
												buf = true;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}
											else {
												buf = false;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}
										}
										
									}
								}
								else
									if (i == 1)												// ���� ��� ������ �������
										if (priority[0] + 1 > 2)							// ���� �� ����� ������ ����
										{
											priority[i - 1] ++;
											spoon_busy[numb_buf] = true;
											buf = true;
											MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
										}

										else
											if (priority[procs_count - 2] + 1 < 2)
											{
												priority[i - 1]++;
												spoon_busy[numb_buf] = true;
												buf = true;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}	// ���� � ������ ��� ������ �����
											else
											{
												buf = false;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}
									else						// ���� ��� ��������� �������
										if (priority[i - 2] + 1 > 2)
										{
											priority[i - 1] ++;
											spoon_busy[numb_buf] = true;
											buf = true;
											MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
										}
										else
											if (priority[0] + 1 < 2)
											{
												priority[i - 1] ++;
												spoon_busy[numb_buf] = true;
												buf = true;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}
											else
											{
												buf = false;
												MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
											}
							}
							else
							{
								buf = false;
								MPI_Send(&buf, 1, MPI_C_BOOL, i, 0, MPI_COMM_WORLD);
							}															//���� ����� ������, �� �� � �� ���
						}


						else {		// �� ������ �� ������������ 2� �����
							if (i != procs_count - 1) {
								spoon_busy[i - 1] = false;								// ����������� ��� �����
								spoon_busy[i] = false;
							}
							else {
								spoon_busy[0] = false;
								spoon_busy[i - 1] = false;
							}
								std::cout << i << " Give spoons " << std::endl;
								priority[i - 1] -= 2;									// ��������� ���������
								food[i]--;
								k++;
							}	
						/*for (int j = 1; j < procs_count; j++) {
							std::cout << food[j] << " | ";
						}
						std::cout << std::endl;*/
						numb_buf = -1;
					}
				}				
			}
		}
		else {								//  ���� ��� �� 0�� �������
			Sleep(2500);
			bool left = false;
			bool right = false;

			int spoon_left = procs_rank - 1;
			int spoon_right = procs_rank;
			if (procs_rank == procs_count - 1) spoon_right = 0;
			for (int i = 0; i < HMEat; i++) {
				while ((!left) || (!right)) {
					if (!left) {				// ���� ��� ����� ����, ����� ���������� �������� �� ���?
						numb_buf = spoon_left;						
						MPI_Send(&numb_buf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);			// ����������� ��������� ����� �����
						MPI_Recv(&buf, 1, MPI_C_BOOL, 0, 0, MPI_COMM_WORLD, &st);				// �������� ���������
						left = buf;
						if (left) std::cout << procs_rank << " Take Left spoon" << std::endl;
						Sleep(500);
					}
					if (!right) {				// ���� ��� ����� ����, ����� ���������� �������� �� ���?
						numb_buf = spoon_right;
						MPI_Send(&numb_buf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);			// ����������� ��������� ����� �����
						MPI_Recv(&buf, 1, MPI_C_BOOL, 0, 0, MPI_COMM_WORLD, &st);				// �������� ���������
						right = buf;
						if (right) std::cout << procs_rank << " Take Right spoon" << std::endl;
						Sleep(500);
					}
					
				}
				Eat(procs_rank);
				numb_buf = -2;
				MPI_Send(&numb_buf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);			// -2 ��������, ��� �� �������� �������������
				left = false;
				right = false;
				Think(procs_rank);
			}
		}
	}
	std::cout << procs_rank << " End" << std::endl;
	MPI_Finalize();

	return 0;
}