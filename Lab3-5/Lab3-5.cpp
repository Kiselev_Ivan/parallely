// copyright by Ivan Kiselev

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <mpi.h> 
#include <windows.h>

const int numb_city = 8;

class Vertex {
private:
	int* distance;

public:
	Vertex() {
		distance = new int(numb_city);
		for (int i = 0; i < numb_city; i++) {
			distance[i] = 0;
		}
	}
	
	~Vertex() {
	};

	bool SetDistanceTo(int numb, int dist) {
		if (dist >= 0) {
			distance[numb] = dist;
			return true;
		}
		else {
			std::cout << " Distance is not correct, please repeat " << std::endl;
			return false;
		}
	}

	int GiveDistanceTo(int numb) {
		return distance[numb];
	}

	void PrintVertex() {
		for (int i = 0; i < numb_city; i++) {
			std::cout << GiveDistanceTo(i) << " ";
		}
		std::cout << std::endl << std::endl;
	}

};

int MIN(int a, int b) {
	return (((a) < (b)) ? (a) : (b));
}

int ShortWay(Vertex** arr, int start, int finish, bool* CompleteV, short* Way, int p = 0) {	// ��������
	
	if (start == finish) {				// ���� �� ������ � ��������� �����.
		Way[p] = finish;
		return 0;
	}
	bool* CompleteVertex = new bool(numb_city);		// ������ ������������ �� ����� �������� ��� ��������,
													// ����� ������� �� ����������� ����

	short* SaveWay = new short(numb_city);
	for (int i = 0; i < numb_city; i++)
		SaveWay[i] = Way[i];

	if (CompleteV == NULL)
		for (int j = 0; j < numb_city; j++)
			CompleteVertex[j] = false;
	else for (int i = 0; i < numb_city; i++)
		CompleteVertex[i] = CompleteV[i];

	CompleteVertex[start] = true;

	int result = arr[start][0].GiveDistanceTo(finish);
	int SW = 0;			// Short Way - ��� ��������������� ��������� ���������
	int road = finish;
	int i = 0;
	while (i < numb_city) {
		if (!CompleteVertex[i]) {
			SW = ShortWay(arr, i, finish, CompleteVertex, SaveWay, p + 1);
			if (result > (arr[start][0].GiveDistanceTo(i) + SW)) {
				result = (arr[start][0].GiveDistanceTo(i) + SW);
				road = i;
				for (int i = 0; i < numb_city; i++)
					Way[i] = SaveWay[i];
			}
		}
		i++;
	}
	Way[p] = road;
	return result;
}


int main(int argc, char* argv[]) {
	
	int rd = 0;					// ������
	srand(time(NULL));
	Vertex** arr = new Vertex*[numb_city];
	for (int i = 0; i < numb_city; i++)
		arr[i] = new Vertex();
	for (int i = 0; i < numb_city; i++)
		for (int j = i + 1; j < numb_city; j++) {
			rd = rand() % 25 + 1;
			arr[i][0].SetDistanceTo(j, rd);
			arr[j][0].SetDistanceTo(i, rd);
		}

	short* Way = new short(numb_city + 1);
	arr[1][0].SetDistanceTo(2, 50);
	arr[2][0].SetDistanceTo(1, 50);

	// �������� ������� ������ � ��� ����������

	

	int procs_count = 0;
	int procs_rank = 0;
	
	MPI_Status st;

	int* buf = new int(3);	// 3 �����: ������,���� � ����� ����������(��� ��������� �������)

	MPI_Init(&argc,&argv);
	MPI_Comm_size(MPI_COMM_WORLD, &procs_count);
	// ���-�� ���������
	MPI_Comm_rank(MPI_COMM_WORLD, &procs_rank);
	// ���� ��������
	
	if (procs_rank == 0) {					// ������� ������� ���������� �������������� � ������ ����������

		for (int i = 0; i < numb_city; i++)	// ������������ ������� 
			arr[i][0].PrintVertex();
		std::cout << std::endl;
		// ����
				buf[0] = 1;
				buf[1] = 2;
				buf[2] = -1;
				MPI_Send(buf, 3, MPI_INT, 1, 0, MPI_COMM_WORLD);
				MPI_Recv(buf, 3, MPI_INT, 1, 0, MPI_COMM_WORLD, &st);
	}
	//  �� �����������, ����� ����� ���������� 0 �����, ����� ����� ������������ � ������� �������
	else {									// ��� ������ ���������
		MPI_Recv(buf, 3, MPI_INT, 0, 0, MPI_COMM_WORLD, &st);
		buf[2] = ShortWay(arr,buf[0],buf[1], NULL, Way, 1);
		MPI_Send(buf, 3, MPI_INT, 0, 0, MPI_COMM_WORLD);
		Way[0] = buf[0];
		Sleep(600);
		std::cout << "Start: " << buf[0] << " Finish: " << buf[1] << " Short Distance: " << buf[2] << std::endl;
		int i = 0;
		while ((Way[i] != buf[1]) && (i < numb_city-1)) {
			std::cout << Way[i] << " -> ";
			i++;
		}
		std::cout << Way[i] << std::endl;
	}		

	MPI_Finalize();
	return 0;
}