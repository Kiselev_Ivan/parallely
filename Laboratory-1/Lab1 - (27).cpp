// Copywriter by Kiselev Ivan

#include <mpi.h> 
#include <iostream>
#include <string.h>

int Min(int a, int b) {
	if (a > b) return b;
	else return a;
}

void SimpleCount(int* k, std::string str1, std::string str2) {
	long unsigned int size1, size2;
	size1 = str1.size();
	size2 = str2.size();
	int min_len = Min(size1,size2);
	for (int i = 0; i < min_len; i++) {
		if (str1[i] != str2[i]) (*k)++;
	}
	k = k + (size1 - min_len) + (size2 - min_len);
}

int CountTheEndOfTheBlock(int block, int blockStart, int length, int procs_rank, int procs_count) {
	if (procs_rank + 2 >= procs_count) return length;
	return blockStart + block;
}


int main(int argc, char* argv[])
{
	FILE* text;
	text = fopen("input.txt","r");
	char buf;
	int Ibuf = 0;
	int block = 0;
	int procs_rank, procs_count;
	int k = 0;
	int blockStart = 0; 
	int blockEnd = 0;
	double start_time, finish_time;
	int shortSize = 0;

	MPI_Status st;
	char str1[256];
	fgets(str1, 255, text);
	char str2[256];
	fgets(str2, 255, text);
	MPI_Init(&argc, &argv);
	// ������������� MPI-���������� 
	MPI_Comm_size(MPI_COMM_WORLD, &procs_count);
	// ���������� ���������� ��������� 
	MPI_Comm_rank(MPI_COMM_WORLD, &procs_rank);
	// ������ ���� �������� 
	start_time = MPI_Wtime();
	std::cout << "Hello from " << procs_rank << std::endl;
	if (procs_count == 1) {
		SimpleCount(&k, str1, str2);
	}
	else {
		shortSize = Min(strlen(str1) - 2, strlen(str2));		
		
		if (procs_count % 2 == 1) {															/*���� ����� ��������� �������*/

			block = (shortSize * 2 / (procs_count - 1));									/* ������ ����� ��� ������ ������*/

			if ((procs_rank % 2) == 1) {													/*���� ��� 1, 3, 5 � ��.*/

				blockStart = (procs_rank - 1) * block / 2;
				blockEnd = CountTheEndOfTheBlock(block, blockStart, shortSize, procs_rank, procs_count);
				for (int i = blockStart; i < blockEnd; i++) {
					MPI_Recv(&buf, 1, MPI_CHAR, procs_rank + 1, 0, MPI_COMM_WORLD, &st);
					if (str1[i] != buf) k++;
				}
			}

			else {																			/*���� ��� 2, 4, 6 � ��.*/
				if (procs_rank != 0) {
					blockStart = (procs_rank - 2) * block / 2;
					blockEnd = CountTheEndOfTheBlock(block, blockStart, shortSize, procs_rank, procs_count);
					for (int i = blockStart; i < blockEnd; i++) {
						buf = str2[i];
						MPI_Send(&buf, 1, MPI_CHAR, procs_rank - 1, 0, MPI_COMM_WORLD);
					}
				}
			}

			if (procs_rank == 0) {																/*���������� ��� ������*/
				k = k + (strlen(str1) - 2 - shortSize) + (strlen(str2) - shortSize);		/*� ������ ������ ������������ /n */
				for (int i = 1; i < procs_count / 2; i++)
				{
					MPI_Recv(&Ibuf, 1, MPI_INT, i * 2 - 1, 0, MPI_COMM_WORLD, &st);
					k = k + Ibuf;
				}
			}
			else {
				if ((procs_rank % 2) == 1) {
					Ibuf = k;
					MPI_Send(&Ibuf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
				}

			}
		}


		else {																					/*���� ����� ��������� �����*/
				block = ( shortSize * 2 / procs_count);											/* ������ ����� ��� ������ ������*/
			
				if ((procs_rank % 2) == 0) {													/*���� ��� 0, 2, 4 � ��.*/
					blockStart = procs_rank * block / 2;
					blockEnd = CountTheEndOfTheBlock(block, blockStart, shortSize, procs_rank, procs_count);
					for (int i = blockStart; i < blockEnd; i++) {
						MPI_Recv(&buf, 1, MPI_CHAR, procs_rank + 1, 0, MPI_COMM_WORLD, &st);
						if (str1[i] != buf) k++;
					}
				}

				else {																			/*���� ��� 1, 3, 5 � ��.*/
					blockStart = (procs_rank - 1) * block / 2;
					blockEnd = CountTheEndOfTheBlock(block, blockStart, shortSize, procs_rank, procs_count);
					for (int i = blockStart; i < blockEnd; i++) {
						buf = str2[i];
						MPI_Send(&buf, 1, MPI_CHAR, procs_rank - 1, 0, MPI_COMM_WORLD);
					}
				}
				
				if (procs_rank == 0) {															/*���������� ��� ������*/
					k = k + (strlen(str1) - 2 - shortSize) + (strlen(str2) - shortSize);		/*� ������ ������ ������������ /n */
					for (int i = 1; i < procs_count / 2; i++)
					{
						MPI_Recv(&Ibuf, 1, MPI_INT, i * 2, 0, MPI_COMM_WORLD, &st);
						k = k + Ibuf;
					}
				}
				else {
					if ((procs_rank % 2) == 0) {
						Ibuf = k;
						MPI_Send(&Ibuf, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
					}
				}
		}

	}
	std::cout << "Process Fineshed : " << procs_rank << std::endl;
	MPI_Finalize();
	if (procs_rank == 0) {
			finish_time = MPI_Wtime();
			std::cout << "Deferent sumbol count = " << k << std::endl;
			std::cout << "Total Time: " << finish_time - start_time << std::endl;
	}
	// ��������� MPI-���������� 
	fclose(text);
	return 0;
}

